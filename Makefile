all: build jar clean

build:
	javac com/glowiak/jsm/Jsm.java

jar:
	jar cvfe jsm.jar com.glowiak.jsm.Jsm `find . -name '*.class'`

clean:
	find . -name '*.class' -delete
