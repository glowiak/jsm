package com.glowiak.jsm;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JComponent;
import javax.swing.ImageIcon;

import java.awt.Toolkit;
import java.awt.Image;
import java.awt.Graphics;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import java.util.ArrayList;

public class Jsm
{
    public static int WIDTH;
    public static int HEIGHT;
    
    public static Jsm me;
    
    private JsmBackground bg;
    
    public JFrame w;
    
    public ArrayList<JButton> MenuButtons;
    public ArrayList<String> ExecC;
    
    public static void main(String[] args)
    {
        if (args.length < 1)
        {
            System.out.println("Error: the config file was not provided");
            System.exit(1);
        } else
        {
            File f = new File(args[0]);
            if (!f.exists())
            {
                System.out.println("Error: the config file does not exist");
                System.exit(1);
            } else
            {
                me = new Jsm(f);
            }
        }
    }
    
    public Jsm(File config)
    {
        MenuButtons = new ArrayList<JButton>();
        ExecC = new ArrayList<String>();
        bg = new JsmBackground();
        
        w = new JFrame();
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.setSize(100, 100);
        w.setUndecorated(true);
        w.setVisible(true);
        w.setLayout(null);
        w.setFocusable(true);
        w.addKeyListener(new JsmKeys());
        w.setContentPane(bg);
        
        try {
            FileReader fr = new FileReader(config.getPath());
            BufferedReader br = new BufferedReader(fr);
            
            String s = null;
            while ((s = br.readLine()) != null)
            {
                String[] s0 = s.split(" ");
                switch (s0[0])
                {
                    case "size":
                        WIDTH = Integer.parseInt(s0[1]);
                        HEIGHT = Integer.parseInt(s0[2]);
                        w.setSize(WIDTH, HEIGHT);
                        w.setLocation(0, Toolkit.getDefaultToolkit().getScreenSize().height - Integer.parseInt(s0[2]));
                        break;
                    case "bg":
                        bg.tex = new ImageIcon(s0[1]).getImage().getScaledInstance(WIDTH, HEIGHT, Image.SCALE_DEFAULT);
                        break;
                    case "button":
                        MenuButtons.add(new JButton(s0[1]));
                        MenuButtons.get(MenuButtons.size() - 1).setBounds(Integer.parseInt(s0[2]), Integer.parseInt(s0[3]), Integer.parseInt(s0[4]), Integer.parseInt(s0[5]));
                        
                        int l = 0;
                        l+= s0[0].length() + 1;
                        l+= s0[1].length() + 1;
                        l+= s0[2].length() + 1;
                        l+= s0[3].length() + 1;
                        l+= s0[4].length() + 1;
                        l+= s0[5].length() + 1;
                        
                        String s1 = s.substring(l, s.length());
                        ExecC.add(s1);
                        
                        MenuButtons.get(MenuButtons.size() - 1).addActionListener(new ActionListener()
                        {
                            public int myC = ExecC.size() - 1;
                            public void actionPerformed(ActionEvent e)
                            {
                                try {
                                    Runtime.getRuntime().exec(ExecC.get(myC));
                                } catch (Exception ex)
                                {
                                    JOptionPane.showMessageDialog(null, "Could not launch specified program.");
                                    ex.printStackTrace();
                                }
                                System.exit(0);
                            }
                        });
                        w.add(MenuButtons.get(MenuButtons.size() - 1));
                        break;
                }
            }
            
            br.close();
            fr.close();
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            System.exit(1);
        }
        while (true)
        {
            w.repaint();
        }
    }
}
class JsmBackground extends JComponent
{
    public Image tex = null;
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        if (tex != null)
        {
            g.drawImage(tex, 0, 0, null);
        }
    }
}
class JsmKeys implements KeyListener
{
    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            System.exit(0);
        }
    }
    public void keyReleased(KeyEvent e){}
    public void keyTyped(KeyEvent e){}
}
